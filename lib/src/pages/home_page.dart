import 'package:flutter/material.dart';
import '../bloc/provider.dart';

class HomePage extends StatelessWidget {
  static const String pageName = 'home';

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Página de Inicio'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Divider(),
          Text('EMail: ${bloc.email}'),
          Divider(),
          Text('Password: ${bloc.password}'),
          Divider(),
        ],
      ),
    );
  }
}
